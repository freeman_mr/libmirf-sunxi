# nRF24L01(+) Cubieboard Java library. #

This is a port of nRF24L01(+) library for Java for use on Allwinner A10/A20 devices (Cubieboard 1/2, Cubietruck, etc.).
It is include C++ sources, java class for connecting library using JNI, examples.

At present it was tested on A10/Cubieboard1.

It is assumed that you have recompiled the kernel with SPI driver support. And you have /dev/spidev0.0 in system.

Connection nrf24l01 to cubieboard:


```
#!list

1 GND -> GND
2 VCC 3.3V -> 3.3V
3 CE - PB10
4 CSN - PB11
5 SCK - PI11
6 MOSI - PI12
7 MISO - PI13
8 IRQ - PH7
```


This library uses IRQ, make sure you have configured IRQ pin in script.fex as extint pin:
```
#!bash

[gpio_para]
gpio_used = 1
gpio_num = 1
gpio_pin_1 = port:PH07<0><default><default><default>
```


To install library unzip it to your /usr/src directory.
Make it:
```
#!bash

 make 
```


Install:
```
#!bash

 make install 
```

Test it:
```
#!bash

cd JavaTest
java -jar testSpiNrf.jar
```

Enjoy!