PREFIX=/usr/local/

LIB=libmirf-sunxi
LIBNAME=$(LIB).so.1.0
OBJDIR=obj/
LIBDIR=lib/

CCFLAGS=-Ofast -mfpu=vfp -mfloat-abi=hard

all:libmirf

libmirf:${OBJDIR}spi.o ${OBJDIR}gpio_sun4i.o ${OBJDIR}Mirf.o ${OBJDIR}SpiNrfDaemon_SPI_MirfNInterface.o
	g++ -shared -Wl,-soname,$@.so.1 ${CCFLAGS} -o ${LIBDIR}${LIBNAME} $^

${OBJDIR}spi.o:src/spi.cpp
	g++ -Wall -fPIC ${CCFLAGS} -c $^ -o $@

${OBJDIR}gpio_sun4i.o:src/gpio_sun4i.cpp
	g++ -Wall -fPIC ${CCFLAGS} -c $^ -o $@

${OBJDIR}Mirf.o:src/Mirf.cpp
	g++ -Wall -fPIC ${CCFLAGS} -DGPIO_SUN4I -c $^ -o $@
	
${OBJDIR}SpiNrfDaemon_SPI_MirfNInterface.o:src/SpiNrfDaemon_SPI_MirfNInterface.cpp
	g++ -I/usr/lib/jvm/java-6-openjdk-armhf/include -lstdc++ -Wall -fPIC ${CCFLAGS} -c $^ -o $@

clean:
	rm -rf ${OBJDIR}*.o ${LIBDIR}${LIBNAME}

install:
	@echo "[Install]"
	@if ( test ! -d ${PREFIX}lib ) ; then mkdir -p ${PREFIX}lib ; fi
	@install -m 0755 ${LIBDIR}${LIBNAME} ${PREFIX}${LIBDIR}
	@ln -sf ${PREFIX}${LIBDIR}${LIBNAME} ${PREFIX}${LIBDIR}${LIB}.so.1
	@ln -sf ${PREFIX}${LIBDIR}${LIB}.so.1 ${PREFIX}${LIBDIR}${LIB}.so
	@ldconfig
