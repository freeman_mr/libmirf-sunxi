

#include "Mirf.h"
// Defines for setting the MiRF registers for transmitting or receiving mode

#include <unistd.h>
#include <sys/time.h>  

static struct timeval start, end;
static long mtime, seconds, useconds;

void __msleep(int milisec)
{
  struct timespec req = {0};
  req.tv_sec = 0;
  req.tv_nsec = milisec * 1000000L;
  nanosleep(&req, (struct timespec *)NULL);
}

void __usleep(int milisec)
{
  struct timespec req = {0};
  req.tv_sec = 0;
  req.tv_nsec = milisec * 1000L;
  nanosleep(&req, (struct timespec *)NULL);
}

void __start_timer()
{
  gettimeofday(&start, NULL);
}

long __millis()
{
	gettimeofday(&end, NULL);
	seconds  = end.tv_sec  - start.tv_sec;
	useconds = end.tv_usec - start.tv_usec;

	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
	return mtime;
}


/****************************************************************************/

void Nrf24::csn(int mode)
{
	gpio->sunxi_gpio_output(csn_pin, mode);
}

/****************************************************************************/

void Nrf24::ce(int mode)
{
	gpio->sunxi_gpio_output(ce_pin, mode);
}


Nrf24::Nrf24(uint16_t _cepin, uint16_t _cspin, string spidev):
  ce_pin(_cepin), csn_pin(_cspin)
{
	_spidev=spidev;
	spi = new SPI(_spidev, 8000000, 8);
	gpio = new GPIO();
}

void Nrf24::initLib()
{

  if (gpio->err != 0)
  {
     return;
  }

  __start_timer();
  // Initialize pins
  int ret = 0;
  ret = gpio->sunxi_gpio_set_cfgpin(ce_pin, OUTPUT);
  if (ret != 0)
  {
     goto cleanup_on_fail;
  }

  ret = gpio->sunxi_gpio_set_cfgpin(csn_pin, OUTPUT);
  if (ret != 0)
  {
     goto cleanup_on_fail;
  }

  ret = gpio->sunxi_gpio_output(csn_pin, HIGH);

  return;

cleanup_on_fail:
  gpio->sunxi_gpio_cleanup();
}

void Nrf24::init() 
// Initializes pins to communicate with the MiRF module
// Should be called in the early initializing phase at startup.
{   
  ce(LOW);
  csn(HIGH);
  // Must allow the radio time to settle else configuration bits will not necessarily stick.
  // This is actually only required following power up but some settling time also appears to
  // be required after resets too. For full coverage, we'll always assume the worst.
  // Enabling 16b CRC is by far the most obvious case if the wrong timing is used - or skipped.
  // Technically we require 4.5ms + 14us as a worst case. We'll just call it 5ms for good measure.
  // WARNING: Delay is based on P-variant whereby non-P *may* require different timing.
  __msleep(25);
}

void Nrf24::transferSync(uint8_t *dataout, uint8_t *datain,uint8_t len){
	uint8_t i;
	for(i = 0;i < len;i++){
		datain[i] = spi->transfer(dataout[i]);
	}
}

void Nrf24::transmitSync(uint8_t *dataout,uint8_t len){
	uint8_t i;
	for(i = 0;i < len;i++){
		spi->transfer(dataout[i]);
	}
}



void Nrf24::config(uint8_t channel, uint8_t pay_length)
// Sets the important registers in the MiRF module and powers the module
// in receiving mode
// NB: channel and payload must be set now.
{
    /* Use static payload length ... */
    payload = pay_length;

    // Set RF channel
    configRegister(RF_CH, channel);

    // Set length of incoming payload 
    configRegister(RX_PW_P0, 0); // Auto-ACK pipe ...
    configRegister(RX_PW_P1, payload); // Data payload pipe
    configRegister(RX_PW_P2, 0x00); // Pipe not used 
    configRegister(RX_PW_P3, 0x00); // Pipe not used 
    configRegister(RX_PW_P4, 0x00); // Pipe not used 
    configRegister(RX_PW_P5, 0x00); // Pipe not used 

    // 1 Mbps, TX gain: 0dbm
    configRegister(RF_SETUP, (0<<RF_DR)|((0x03)<<RF_PWR));

    // CRC enable, 1 byte CRC length
    configRegister(CONFIG, mirf_CONFIG);

    // Auto Acknowledgment
    configRegister(EN_AA,(1<<ENAA_P0)|(1<<ENAA_P1)|(0<<ENAA_P2)|(0<<ENAA_P3)|(0<<ENAA_P4)|(0<<ENAA_P5));

    // Enable RX addresses
    configRegister(EN_RXADDR,(1<<ERX_P0)|(1<<ERX_P1)|(0<<ERX_P2)|(0<<ERX_P3)|(0<<ERX_P4)|(0<<ERX_P5));

    // Auto retransmit delay: 1250 us and Up to 10 retransmit trials
   
     configRegister(SETUP_RETR,(0x06<<ARD)|(0x0A<<ARC));

    // Dynamic length configurations: No dynamic length
    configRegister(DYNPD,(0<<DPL_P0)|(0<<DPL_P1)|(0<<DPL_P2)|(0<<DPL_P3)|(0<<DPL_P4)|(0<<DPL_P5));

    // Start listening
    powerUpRx();
}

void Nrf24::setRADDR(uint8_t * adr) 
// Sets the receiving address
{
	ce(LOW);
	writeRegister(RX_ADDR_P1,adr,mirf_ADDR_LEN);
	ce(LOW);
}

void Nrf24::setTADDR(uint8_t * adr)
// Sets the transmitting address
{
	/*
	 * RX_ADDR_P0 must be set to the sending addr for auto ack to work.
	 */

	writeRegister(RX_ADDR_P0,adr,mirf_ADDR_LEN);
	writeRegister(TX_ADDR,adr,mirf_ADDR_LEN);
}

bool Nrf24::dataReady() 
// Checks if data is available for reading
{
    // See note in getData() function - just checking RX_DR isn't good enough
	uint8_t status = getStatus();

    // We can short circuit on RX_DR, but if it's not set, we still need
    // to check the FIFO for any pending packets
    if ( status & (1 << RX_DR) ) 
	{
		return 1;
	}
    return !rxFifoEmpty();
}

bool Nrf24::rxFifoEmpty(){
	uint8_t fifoStatus;

	readRegister(FIFO_STATUS,&fifoStatus,1);
	return (fifoStatus & (1 << RX_EMPTY));
}



void Nrf24::getData(uint8_t * data) 
// Reads payload bytes into data array
{
	for(uint8_t i =0; i<payload; i++)
		data[i] = 0xff;
    csn(LOW);                               // Pull down chip select
    spi->transfer( R_RX_PAYLOAD );            // Send cmd to read rx payload
    transferSync(data,data,payload); // Read payload
    csn(HIGH);                               // Pull up chip select
    // NVI: per product spec, p 67, note c:
    //  "The RX_DR IRQ is asserted by a new packet arrival event. The procedure
    //  for handling this interrupt should be: 1) read payload through SPI,
    //  2) clear RX_DR IRQ, 3) read FIFO_STATUS to check if there are more 
    //  payloads available in RX FIFO, 4) if there are more data in RX FIFO,
    //  repeat from step 1)."
    // So if we're going to clear RX_DR here, we need to check the RX FIFO
    // in the dataReady() function
    configRegister(STATUS,(1<<RX_DR));   // Reset status register
}

void Nrf24::configRegister(uint8_t reg, uint8_t value)
// Clocks only one byte into the given MiRF register
{
    csn(LOW);
    spi->transfer(W_REGISTER | (REGISTER_MASK & reg));
    spi->transfer(value);
    csn(HIGH);
}

void Nrf24::readRegister(uint8_t reg, uint8_t * buf, uint8_t len)
// Reads an array of bytes from the given start position in the MiRF registers.
{

	  csn(LOW);
	  spi->transfer(R_REGISTER | (REGISTER_MASK & reg));
	  while (len--)
		*buf++ = spi->transfer(0xff);

	  csn(HIGH);
}

void Nrf24::writeRegister(uint8_t reg, uint8_t * buf, uint8_t len) 
// Writes an array of bytes into inte the MiRF registers.
{

	  csn(LOW);
	  spi->transfer( W_REGISTER | ( REGISTER_MASK & reg ) );
	  while ( len-- )
		spi->transfer(*buf++);

	  csn(HIGH);
}


void Nrf24::send(uint8_t * value) 
// Sends a data package to the default address. Be sure to send the correct
// amount of bytes as configured as payload on the receiver.
{
    ce(LOW);
     
    /* Set to transmitter mode , Power up if needed */
    powerUpTx();

    csn(LOW);                    // Pull down chip select
    spi->transfer( FLUSH_TX );     // Write cmd to flush tx fifo
    csn(HIGH);                    // Pull up chip select
    
    csn(LOW);                    // Pull down chip select
    spi->transfer( W_TX_PAYLOAD ); // Write cmd to write payload
    transmitSync(value,payload);   // Write payload
    csn(HIGH);     

    /* Start the transmission */
	  ce(HIGH);
	  __usleep(15);
	  ce(LOW);
}


// Test if chip is still sending.
// When sending has finished return chip to listening.
bool Nrf24::isSending(){
    uint8_t status;

    /* read the current status */
    status = getStatus();
                
    /* if sending successful (TX_DS) or max retries exceded (MAX_RT). */
    if((status & ((1 << TX_DS)  | (1 << MAX_RT))))
    {        
        return 0; /* false */
    }

    return 1; /* true */
}

uint8_t Nrf24::getStatus(){
	uint8_t rv;
	 readRegister(STATUS,&rv,1);
	return rv;
}

uint8_t Nrf24::lastMessageStatus()
{
    uint8_t rv;

    rv = getStatus();

    /* Transmission went OK */
    if((rv & ((1 << TX_DS))))
    {
        return NRF24_TRANSMISSON_OK;
    }
    /* Maximum retransmission count is reached */
    /* Last message probably went missing ... */
    else if((rv & ((1 << MAX_RT))))
    {
        return NRF24_MESSAGE_LOST;
    }  
    /* Probably still sending ... */
    else
    {
        return 0xFF;
    }
}

/* Returns the number of retransmissions occured for the last message */
uint8_t Nrf24::retransmissionCount()
{
    uint8_t rv;
    readRegister(OBSERVE_TX, &rv,1);
    rv = rv & 0x0F;
    return rv;
}

void Nrf24::powerUpRx(){
	PTX = 0;

	configRegister(STATUS,(1 << TX_DS) | (1 << MAX_RT)); 

	ce(LOW);
	configRegister(CONFIG, mirf_CONFIG | ( (1<<PWR_UP) | (1<<PRIM_RX) ) );
	ce(HIGH);
	__usleep(130);
}

void Nrf24::flushRx(){
    csn(LOW);
    spi->transfer( FLUSH_RX );
    csn(HIGH);
}

void Nrf24::powerUpTx(){
	configRegister(STATUS,(1<<RX_DR)|(1<<TX_DS)|(1<<MAX_RT)); //????
	PTX = 1;
	configRegister(CONFIG, mirf_CONFIG | ( (1<<PWR_UP) | (0<<PRIM_RX) ) );
	__usleep(150);
}

void Nrf24::powerDown(){
	ce(LOW);
	configRegister(CONFIG, mirf_CONFIG );
}


void Nrf24::whatHappened(bool& tx_ok,bool& tx_fail,bool& rx_ready)
{
	whatHappened(tx_ok, tx_fail, rx_ready, NULL);
}

void Nrf24::whatHappened(bool& tx_ok,bool& tx_fail,bool& rx_ready, uint8_t* pipe_num)
{
	/* from Nrf2401 datasheet:
	1) read payload
	2) clear RX_RD
	3) read RX FIFO status
	4) repeat from 1 if there is data in RX FIFO

	So no more interrupt clearing here.
	*/

  uint8_t status = getStatus();

  // Report to the user what happened
  tx_ok = status & _BV(TX_DS);
  tx_fail = status & _BV(MAX_RT);
  rx_ready = status & _BV(RX_DR);

  if (pipe_num){ //copy pipe number
    *pipe_num = (status >> RX_P_NO) & 0x07;// 0b111;
  }
}

void Nrf24::clearInterrupt()
{
	uint8_t buf = _BV(RX_DR) | _BV(TX_DS) | _BV(MAX_RT);

	writeRegister(STATUS, &buf, 1);
}
