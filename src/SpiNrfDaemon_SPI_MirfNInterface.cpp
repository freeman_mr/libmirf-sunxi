
#include "SpiNrfDaemon_SPI_MirfNInterface.h"

#include "gpio_sun4i.h"
#include <cstdlib>
#include <iostream>
#include "Mirf.h"
#include <stdio.h>

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>
#include <pthread.h>

#define USE_IRQ

Nrf24 nrf(SUNXI_GPB(10), SUNXI_GPB(11), "/dev/spidev0.0");


#ifdef USE_IRQ

#define SYSFS_GPIO_DIR "/sys/class/gpio"
#define GPIO_STR "gpio1_ph7"
int int_gpio_num = 1;

#define MAX_BUF 64
int gpio_fd;

JavaVM* mJVM;
pthread_t hPollingThread;


/****************************************************************
 * gpio_export
 ****************************************************************/
int gpio_export(unsigned int gpio)
{
	int fd, len;
	char buf[MAX_BUF];
	fd = open(SYSFS_GPIO_DIR "/export", O_WRONLY);
	if (fd < 0) {
		perror("gpio/export");
		return fd;
	}
	len = snprintf(buf, sizeof(buf), "%d", gpio);
	write(fd, buf, len);
	close(fd);
	return 0;
}

/****************************************************************
 * gpio_set_edge
 ****************************************************************/
int gpio_set_edge(char *gpio, char *edge, char* active_low)
{
	int fd;
	char buf[MAX_BUF];
	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/%s/edge", gpio);
	fd = open(buf, O_WRONLY);
	if (fd < 0) {
		perror("gpio/set-edge");
		return fd;
	}
	write(fd, edge, strlen(edge) + 1); 
	close(fd);
	// set active low
	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/%s/active_low", gpio);
	fd = open(buf, O_WRONLY);
	if (fd < 0) {
		perror("gpio/set-active_low");
		return fd;
	}	
	write(fd, active_low, strlen(active_low) + 1); 
	close(fd);		
	return 0;
}

/****************************************************************
 * gpio_fd_open
 ****************************************************************/
int gpio_fd_open(char *gpio)
{
	int fd;
	char buf[MAX_BUF];
	snprintf(buf, sizeof(buf), SYSFS_GPIO_DIR "/%s/value", gpio);
	fd = open(buf, O_RDONLY | O_NONBLOCK );
	if (fd < 0) 
		perror("gpio/fd_open");
	return fd;
}

/****************************************************************
 * gpio_fd_close
 ****************************************************************/
int gpio_fd_close(int fd)
{
	return close(fd);
}


void gotData()
{
    JNIEnv * g_env;
	jmethodID method;
	jclass mCls;


    int getEnvStat = mJVM->GetEnv((void **)&g_env, JNI_VERSION_1_6);
    if (getEnvStat == JNI_EDETACHED) 
	{
        if (mJVM->AttachCurrentThread((void **) &g_env, NULL) != 0) 
		{
        }
    } 
	getEnvStat = mJVM->GetEnv((void **)&g_env, JNI_VERSION_1_6);
	if (getEnvStat == JNI_OK) 
	{
		mCls = g_env->FindClass("SpiNrfDaemon/SPI_MirfNInterface");
		if (g_env->ExceptionCheck()) {
			pthread_cancel(hPollingThread);
		   return ;
		}
		method = g_env->GetStaticMethodID(mCls, "ConnectionCallback", "()V");
		if (g_env->ExceptionCheck()) {
			pthread_cancel(hPollingThread);	
		   return ;
		}
		g_env->CallStaticVoidMethod( mCls, method);
		if (g_env->ExceptionCheck()) {
			pthread_cancel(hPollingThread);
		   return ;
		}
	}
	else
		pthread_cancel(hPollingThread);

    mJVM->DetachCurrentThread();
}

void *PollingThread(void * id)
{
	// setup interrupt
	gpio_export(int_gpio_num);
	gpio_set_edge((char*)GPIO_STR, (char*)"rising", (char*)"1");

	gpio_fd = gpio_fd_open((char*)GPIO_STR);
	struct pollfd fdset[1];
	int nfds = 1;
	int rc, timeout;
	char *buf[MAX_BUF];
	timeout = -1;
	while(1)
	{
			memset((void*)fdset, 0, sizeof(fdset));
			fdset[0].fd = gpio_fd;
			fdset[0].events = POLLPRI;
			rc = poll(fdset, nfds, timeout);      
			if (rc < 0) {
				pthread_cancel(hPollingThread);
				printf("\npoll() failed!\n");
				pthread_exit(NULL);
			}
			if (rc == 0) {
				//printf(".");
			}
			if (fdset[0].revents & POLLPRI) {
				read(fdset[0].fd, buf, MAX_BUF);
				gotData();
			}
			//fflush(stdout);
	}
	gpio_fd_close(gpio_fd);
    pthread_exit(NULL);
}

#endif




/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    init
 * Signature: ()V
 */
JNIEXPORT jboolean JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_init
  (JNIEnv *env, jclass cls)
{
	nrf.initLib();
	nrf.init();

#ifdef USE_IRQ
	env->GetJavaVM(&mJVM);
	int rc;
	pthread_t pollingThread;
	int id;
	rc = pthread_create(&pollingThread, NULL, PollingThread, &id);
	if(rc != 0)
		return JNI_FALSE;
#endif
	return JNI_TRUE;
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    config
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_config
  (JNIEnv *, jclass, jint channel, jint len)
{
	if((uint8_t)channel > 125)
		channel = 125;
	if((uint8_t)len > 32)
		len = 32;
	nrf.config((uint8_t)channel, (uint8_t)len);
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    send
 * Signature: ([B)V
 */
JNIEXPORT void JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_send
  (JNIEnv * env, jclass, jbyteArray bufArray)
{
	jbyte* pBuf			= env->GetByteArrayElements(bufArray, 0);
	nrf.send((uint8_t*)pBuf);
	env->ReleaseByteArrayElements(bufArray, pBuf, 0);
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    setRADDR
 * Signature: ([B)V
 */
JNIEXPORT void JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_setRADDR
  (JNIEnv * env, jclass, jbyteArray bufArray)
{
	jbyte* pBuf			= env->GetByteArrayElements(bufArray, 0);
	nrf.setRADDR((uint8_t*)pBuf);
	env->ReleaseByteArrayElements(bufArray, pBuf, 0);
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    setTADDR
 * Signature: ([B)V
 */
JNIEXPORT void JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_setTADDR
(JNIEnv * env, jclass, jbyteArray bufArray)
{
	jbyte* pBuf			= env->GetByteArrayElements(bufArray, 0);
	nrf.setTADDR((uint8_t*)pBuf);
	env->ReleaseByteArrayElements(bufArray, pBuf, 0);
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    dataReady
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_dataReady
  (JNIEnv *, jclass)
{
	if(nrf.dataReady() == 0)
		return JNI_FALSE;
	else
		return JNI_TRUE;		
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    isSending
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_isSending
  (JNIEnv *, jclass)
{
	if(nrf.isSending() == 0)
		return JNI_FALSE;
	else
		return JNI_TRUE;
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    rxFifoEmpty
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_rxFifoEmpty
  (JNIEnv *, jclass)
{
	if(nrf.rxFifoEmpty() == 0)
		return JNI_FALSE;
	else
		return JNI_TRUE;
}
/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    txFifoEmpty
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_txFifoEmpty
  (JNIEnv *, jclass)
{
	if(nrf.txFifoEmpty() == 0)
		return JNI_FALSE;
	else
		return JNI_TRUE;
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    getData
 * Signature: ([B)V
 */
JNIEXPORT void JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_getData
  (JNIEnv *env, jclass, jbyteArray bufArray)
{
	jbyte* pBuf	= env->GetByteArrayElements(bufArray, 0);
	nrf.getData((uint8_t*)pBuf);
	env->ReleaseByteArrayElements(bufArray, pBuf, 0);
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    getStatus
 * Signature: ()B
 */
JNIEXPORT jbyte JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_getStatus
  (JNIEnv *, jclass)
{
	return (jbyte)nrf.getStatus();
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    lastMessageStatus
 * Signature: ()B
 */
JNIEXPORT jbyte JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_lastMessageStatus
  (JNIEnv *, jclass)
{
	return (jbyte)nrf.lastMessageStatus();
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    retransmissionCount
 * Signature: ()B
 */
JNIEXPORT jbyte JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_retransmissionCount
  (JNIEnv *, jclass)
{
	return (jbyte)nrf.retransmissionCount();
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    transmitSync
 * Signature: ([BI)V
 */
JNIEXPORT void JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_transmitSync
  (JNIEnv * env, jclass, jbyteArray bufArray, jint len)
{
	jbyte* pBuf	= env->GetByteArrayElements(bufArray, 0);
	nrf.transmitSync((uint8_t*)bufArray, len);
	env->ReleaseByteArrayElements(bufArray, pBuf, 0);
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    transferSync
 * Signature: ([B[BI)V
 */
JNIEXPORT void JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_transferSync
  (JNIEnv *env , jclass, jbyteArray inBuf, jbyteArray outBuf, jint len)
{
	jbyte* pInBuf	= env->GetByteArrayElements(inBuf, 0);
	jbyte* pOutBuf	= env->GetByteArrayElements(outBuf, 0);

	nrf.transferSync((uint8_t*)pInBuf, (uint8_t*)pOutBuf, len);

	env->ReleaseByteArrayElements(inBuf, pInBuf, 0);
	env->ReleaseByteArrayElements(outBuf, pOutBuf, 0);
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    configRegister
 * Signature: (BB)V
 */
JNIEXPORT void JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_configRegister
  (JNIEnv *, jclass, jbyte reg, jbyte val)
{
	nrf.configRegister((uint8_t)reg, (uint8_t)val);
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    readRegister
 * Signature: (B[BI)V
 */
JNIEXPORT void JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_readRegister
  (JNIEnv *env , jclass, jbyte reg, jbyteArray bufArray, jint len )
{
	jbyte* pBuf	= env->GetByteArrayElements(bufArray, 0);
	nrf.readRegister(reg, (uint8_t*)pBuf, len);
	env->ReleaseByteArrayElements(bufArray, pBuf, 0);
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    writeRegister
 * Signature: (B[BI)V
 */
JNIEXPORT void JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_writeRegister
  (JNIEnv *env, jclass, jbyte reg, jbyteArray bufArray, jint len)
{
	jbyte* pBuf	= env->GetByteArrayElements(bufArray, 0);
	nrf.writeRegister(reg, (uint8_t*)bufArray, len);
	env->ReleaseByteArrayElements(bufArray, pBuf, 0);
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    powerUpRx
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_powerUpRx
  (JNIEnv *, jclass)
{
	nrf.powerUpRx();
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    powerUpTx
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_powerUpTx
  (JNIEnv *, jclass)
{
	nrf.powerUpTx();
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    powerDown
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_powerDown
  (JNIEnv *, jclass)
{
	nrf.powerDown();
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    flushRx
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_flushRx
  (JNIEnv *, jclass)
{
	nrf.flushRx();
}

/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    whatHappened
 * Signature: ([Z[Z[Z[I)V
 */
JNIEXPORT void JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_whatHappened
  (JNIEnv * env, jclass, jbooleanArray txOkObj, jbooleanArray txFailObj, jbooleanArray rxReadyObj, jintArray pipeNumObj)
{
	jboolean* txOk		= env->GetBooleanArrayElements(txOkObj, 0);
	jboolean* txFail	= env->GetBooleanArrayElements(txFailObj, 0);
	jboolean* rxReady	= env->GetBooleanArrayElements(rxReadyObj, 0);
	jint* pipeNumO		= env->GetIntArrayElements(pipeNumObj, 0);
	
	bool ok, fail, ready;
	uint8_t pipe;
	nrf.whatHappened(ok, fail, ready, &pipe);

	if(ok == true)
		*txOk = JNI_TRUE;
	else
		*txOk = JNI_FALSE;

	if(fail == true)
		*txFail = JNI_TRUE;
	else
		*txFail = JNI_FALSE;

	if(ready == true)
		*rxReady = JNI_TRUE;
	else
		*rxReady = JNI_FALSE;
	*pipeNumO = pipe;
	
	env->ReleaseBooleanArrayElements(txOkObj, txOk, 0);
	env->ReleaseBooleanArrayElements(txFailObj, txFail, 0);
	env->ReleaseBooleanArrayElements(rxReadyObj, rxReady, 0);
	env->ReleaseIntArrayElements(pipeNumObj, pipeNumO, 0);

}
/*
 * Class:     SpiNrfDaemon_SPI_MirfNInterface
 * Method:    clearInterrupt
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_SpiNrfDaemon_SPI_1MirfNInterface_clearInterrupt
  (JNIEnv *, jclass)
{
	nrf.clearInterrupt();
}


