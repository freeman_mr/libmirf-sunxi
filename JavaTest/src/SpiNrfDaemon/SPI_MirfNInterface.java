/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SpiNrfDaemon;

import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import testspinrf.TestSpiNrf;



/**
 *
 * @author ikholiavko
 */
public  class SPI_MirfNInterface {
    
    enum registers{CONFIG, EN_AA,EN_RXADDR,SETUP_AW, SETUP_RETR, RF_CH, RF_SETUP,STATUS,
    OBSERVE_TX, CD, RX_ADDR_P0, RX_ADDR_P1, RX_ADDR_P2, RX_ADDR_P3, RX_ADDR_P4, RX_ADDR_P5,
    TX_ADDR, RX_PW_P0, RX_PW_P1, RX_PW_P2, RX_PW_P3, RX_PW_P4, RX_PW_P5, FIFO_STATUS};

    public static synchronized String printRegisters()
    {
        
        StringBuilder sb = new StringBuilder();
        byte[] reg = new byte[5];
        for(int i = 0; i<24; i++)
        {
            sb.append(String.format("0x%02x", i & 0xff) + "\t");
            sb.append(registers.values()[i].name());
            sb.append(":\t");
            if(i == 0x0A || i == 0x0B || i == 0x10)
            {
                readRegister((byte)i, reg, 5);
                sb.append(String.format("%02x", reg[0] & 0xff));         
                sb.append(String.format("%02x", reg[1] & 0xff));    
                sb.append(String.format("%02x", reg[2] & 0xff));    
                sb.append(String.format("%02x", reg[3] & 0xff));    
                sb.append(String.format("%02x", reg[4] & 0xff));    
            }
            else
            {
                readRegister((byte)i, reg, 1);
                sb.append(String.format("%02x", reg[0] & 0xff));
            }
            sb.append("\n");
        }   
        readRegister((byte)0x1c, reg, 1);
        sb.append("0x1c\t");
        sb.append("DYNPD");
        sb.append(":\t");
        sb.append(String.format("%02x", reg[0] & 0xff));
        sb.append("\n");
        
        readRegister((byte)0x1d, reg, 1);
        sb.append("0x1d\t");
        sb.append("FeATURE");
        sb.append(":\t");
        sb.append(String.format("%02x", reg[0] & 0xff));
        sb.append("\n");
        return sb.toString();     
    }
    
    static {
      //  System.loadLibrary("librf24-sunxi-jni");
        System.load("/usr/local/lib/libmirf-sunxi.so");
    }  
    public static void ConnectionCallback()
    {
        synchronized(SPI_MirfNInterface.class)
        {         
            if(handler != null)
                handler.NewDataReceived();
        
        }
    }    
    
private static NewDataHandler handler;

public interface NewDataHandler    
{
    public void NewDataReceived();
}
    
public static void SetNewDataHandler(NewDataHandler handler)
{
    SPI_MirfNInterface.handler = handler;
}

public static synchronized  native boolean init();

public static synchronized native void config(int channel, int payload_len);
public static synchronized native void send(byte[] data);
public static synchronized native void setRADDR(byte[] adr);
public static synchronized native void setTADDR(byte[] adr);
public static synchronized native boolean dataReady();
public static synchronized native boolean isSending();
public static synchronized native boolean rxFifoEmpty();
public static synchronized native boolean txFifoEmpty();
public static synchronized native void getData(byte[] data);
public static synchronized native byte getStatus();
public static synchronized native byte lastMessageStatus();
public static synchronized native byte retransmissionCount();

public static synchronized native void transmitSync(byte[] data, int len);
public static synchronized native void transferSync(byte[] dataout, byte[] datain,int len);
public static synchronized native void configRegister(byte reg, byte value);
public static synchronized native void readRegister(byte reg, byte[] data, int len);
public static synchronized native void writeRegister(byte reg, byte[] data, int len);
public static synchronized native void powerUpRx();
public static synchronized native void powerUpTx();
public static synchronized native void powerDown();

public static synchronized native void flushRx();

public static synchronized native void whatHappened(boolean[] tx_ok, boolean[]  tx_fail, boolean[]  rx_ready, int[] pipe_num);
public static synchronized native void clearInterrupt();	
}
