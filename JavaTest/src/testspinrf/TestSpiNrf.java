/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testspinrf;

import SpiNrfDaemon.SPI_MirfNInterface;
import SpiNrfDaemon.SPI_MirfNInterface.NewDataHandler;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author ikholiavko
 */
public class TestSpiNrf{
    
    private static class NewData implements NewDataHandler
    {
    
        @Override
        public void NewDataReceived() {
            boolean[] tx_ok = new boolean[1];
            boolean[] tx_fail = new boolean[1];
            boolean[] rx_ready = new boolean[1];
            int[] pipe_num = new int[1];
            SPI_MirfNInterface.whatHappened(tx_ok, tx_fail, rx_ready, pipe_num);
            if(tx_ok[0] == true)
                System.out.println("TX OK");
            if(tx_fail[0] == true)
                System.out.println("tx_fail");
            if(rx_ready[0] == true)
            {
                byte[] receivePayload = new byte[32];   
                boolean isDataReady = true;
                while(isDataReady)
                {
                    SPI_MirfNInterface.getData(receivePayload); 
                    String decoded = new String();
                    try {
                        decoded = new String(receivePayload, "UTF-8");
                    } catch (UnsupportedEncodingException ex) {
                        Logger.getLogger(TestSpiNrf.class.getName()).log(Level.SEVERE, null, ex);
                    }	
                    System.out.println("Received data:" + decoded);       
                    isDataReady = SPI_MirfNInterface.dataReady();
                }
            }
        }
    }
    
    public static void main(String[] args) throws InterruptedException, UnsupportedEncodingException, IOException {
        System.out.println("Is init OK:" + SPI_MirfNInterface.init());
        // Channel 70; Payload: 32
        SPI_MirfNInterface.config(70, 32);
        byte[] tx_mac = new byte[5];
        byte[] rx_mac = new byte[5];

        rx_mac[0] = (byte)0xC7;
        rx_mac[1] = (byte)0xC7;
        rx_mac[2] = (byte)0xC7;
        rx_mac[3] = (byte)0x01;
        rx_mac[4] = (byte)0x01;

        tx_mac[0] = (byte)0xC7;
        tx_mac[1] = (byte)0xC7;
        tx_mac[2] = (byte)0xC7;
        tx_mac[3] = (byte)0xff;
        tx_mac[4] = (byte)0xff;
    
        SPI_MirfNInterface.setTADDR(tx_mac);
        SPI_MirfNInterface.setRADDR(rx_mac);
        SPI_MirfNInterface.flushRx();
        NewData handler = new NewData();
        SPI_MirfNInterface.SetNewDataHandler(handler);
        SPI_MirfNInterface.powerUpRx();
        
        int cntr = 0;     
        System.out.println( SPI_MirfNInterface.printRegisters() );
        
        while(true)
        {
            System.in.read();
            
            byte[] sendPayload = new byte[32];
            String message = "Test: " + Integer.toString(cntr);
            byte[] charArray = message.getBytes(Charset.forName("UTF-8"));
            for(int i = 0; i< charArray.length; i++)
                sendPayload[i] = charArray[i];

            int status;
            synchronized(SPI_MirfNInterface.class)
            {
                System.out.print("Sending");
                
                SPI_MirfNInterface.setTADDR(tx_mac);
                SPI_MirfNInterface.send(sendPayload); 
                while(SPI_MirfNInterface.isSending()){
                    System.out.print(".");
                }
               System.out.print("\n");
                status = SPI_MirfNInterface.lastMessageStatus();
                SPI_MirfNInterface.powerUpRx();
            }
            if(status == 0)
            {
                System.out.print("ACK OK");
            }
            else if(status == 1)
            {  
                System.out.print("NO ACK");
            }  
            else
            {  
                System.out.print("Some error");
            }  
            System.out.println("\t Used retransmissions:" + SPI_MirfNInterface.retransmissionCount());
            cntr++;
            
        }
        
    }

    
}
